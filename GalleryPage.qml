import QtQuick 2.0
import QtQuick.Controls.Material 2.12
import QtQuick.Controls 2.4

Item {
    ListModel{
        id: gallery_model
        Component.onCompleted: {
            var urls = []
            for(var i = 1; i < 10; i++)
                urls.push("file:///home/kiops/dev/mfd/images/"+ i.toString() + ".jpg")
            for(var i = 0; i < 9; i++)
                gallery_model.append({"image_url": urls[i]})
        }
    }
    
    GridView{
        id: gridView
        header: Label{
            text: "Photos near your location:"
            font.pixelSize: 22
        }

        anchors.centerIn: parent
        width: parent.width*0.95
        height: parent.height*0.95
        model: gallery_model
        cellWidth: width/2
        cellHeight: height/3
        delegate: Item{
            width: gridView.cellWidth
            height: gridView.cellHeight
            property bool isSelected: false
            Rectangle{
                anchors.fill: parent
                color: "#26A69A"
                visible: parent.isSelected
                width: gridView.cellWidth
                height: gridView.cellHeight
            }
            Image{
                anchors.centerIn: parent
                width: parent.width *0.97
                height: parent.height*0.97
                source: image_url
                fillMode: Image.PreserveAspectCrop
                MouseArea{
                    anchors.fill: parent
                    onClicked: parent.parent.isSelected = !parent.parent.isSelected
                }
            }
        }
    }

    //    Image{
    //        anchors.centerIn: parent
    //        source: gallery_model.get(0).image_url
    //    }
}
