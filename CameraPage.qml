import QtQuick 2.0
import QtMultimedia 5.12
import QtQuick.Controls 2.5

Item {
    id: page
    //    Text {
    //        id: text
    //        text: qsTr("text")
    //        anchors.centerIn: parent
    //    }
    property bool switcher: false
    
    Video {
        anchors.fill: parent
        id: video
        source: "file:///home/kiops/dev/build-haze_mockup-Desktop_Qt_5_12_2_CLang_64bit2-Debug/theater_clear.mp4"
        volume: 0.0
        autoPlay: true
        visible: !switcher
        onStopped: play()
        
        //        MouseArea {
        //            anchors.fill: parent
        //            onClicked: {
        //                video.play()
        //            }
        //        }
        fillMode: VideoOutput.PreserveAspectCrop 
        
        focus: true
    }
    Video {
        anchors.fill: parent
        id: video2
        source: "file:///home/kiops/dev/build-haze_mockup-Desktop_Qt_5_12_2_CLang_64bit2-Debug/theater_collide_2.mp4"
        autoPlay: true
        volume: 0.0
        visible: switcher
        fillMode: VideoOutput.PreserveAspectCrop 
        onStopped: play()
        //        MouseArea {
        //            anchors.fill: parent
        //            onClicked: {
        //                video.play()
        //            }
        //        }
        
        focus: true
        MouseArea{
            anchors.fill: parent
            onClicked: switcher = false
        }
    }

    
    Rectangle{
        visible: !switcher
        color: "white"
        width: 50
        radius: 100
        height: width
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.margins: 5
    Rectangle{
        color: "#26A69A"
        width: parent.width-6
        radius: 100
        height: width
        anchors.centerIn: parent

        Rectangle{
            width: parent.width-2
            radius: 100
            height: width
            anchors.centerIn: parent
        }
    }
    MouseArea{
        anchors.fill: parent
        onClicked: timer.start()
    }
    }

    Rectangle{
        visible: switcher
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        height: parent.height/2
        color: "white"
        anchors.margins: 5
        opacity: 0.6
        radius: 50
        width: 15
        Slider{
            anchors.centerIn: parent
            height: parent.height
            value: 0.5
            orientation: Qt.Vertical
        }
    }
    Timer{
        id: timer 
        interval: 2000
        onTriggered: page.switcher = true
    }
    
    Rectangle{
        id: await
        visible: timer.running
        anchors.centerIn: parent
        width: 150
        height: 120
        color: "White"
        radius: 5
        opacity: 0.9
        BusyIndicator{
            id: busy
            running: await.visible
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.margins: 5
        }
        Label{
            id: lab
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.top: busy.bottom
            anchors.bottom: parent.bottom
            anchors.margins: 5
            width: parent.width * 0.95
            text: "Hold right that, please. Applying merge.."
            wrapMode: Text.WordWrap 
        }
    }
}
