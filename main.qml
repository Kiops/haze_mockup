import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Dialogs 1.2

ApplicationWindow {
    visible: true
    width: 620
    height: 360
//    width: 360
//    height: 620
    title: qsTr("Tabs")

//    Rectangle{
//        anchors.fill: parent
//        id: rec
//        color: "green"
//        Text {
//            id: name
//            anchors.centerIn: parent
//            text: (rec.width).toString() + (rec.height).toString()
//        }
//    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        
        CameraPage {
            
        }
        
        GalleryPage {
            
        }
        
        MapPage {
            
        }
        
        
    }
    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        
        TabButton {
            text: qsTr("Camera")
        }
        TabButton {
            text: qsTr("Gallery")
        }
        TabButton {
            text: qsTr("Map")
        }
    }
}
